import React, { useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import useCachedResources from './src/hooks/useCachedResources';
import useColorScheme from './src/hooks/useColorScheme';
import Navigation from './src/navigation';


import { AppDataSource } from './src/database/config';
import { User } from './src/database/entities/User.entity'


export default function App(props : any) {
    useEffect(() => {
        if(!AppDataSource.isInitialized){
            AppDataSource.initialize()
            .then(() => console.log("Database initialization SUCESS"))
            .catch((err) => console.log("Database initialization FAILED", err))
            .finally(async () => {
            })
        } else {
            console.log("Database initialization ALREADY")
        }
        async function wrapper() {
            let repository = AppDataSource.getRepository(User)
            let user = new User()
            user.firstName = "Francisco"
            user.lastName = "Pena"
            user.age = 24
            await repository.save(user)
        }
        wrapper()

        async function wrapper2() {
            let repository = AppDataSource.getRepository(User)
            const allUsers = await repository.find()
            console.log(allUsers)
        }
    })


    const isLoadingComplete = useCachedResources();
    const colorScheme = useColorScheme();

    if (!isLoadingComplete) {
        return null;
    } else {
        return (
            <SafeAreaProvider>
            <Navigation colorScheme={colorScheme} />
            <StatusBar />
            </SafeAreaProvider>
            );
    }
}
