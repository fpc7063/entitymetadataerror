import "reflect-metadata"
import * as SQLite from 'expo-sqlite'
import { DataSource, DataSourceOptions } from "typeorm/browser"

import { User } from "./entities/User.entity"


export const AppDataSourceOptions: DataSourceOptions = {
    type: 'expo',
    database: 'zipzop.db',
    driver: SQLite,
    synchronize: true,
    logging: false,
    entities: [User],
    migrations: [],
    subscribers: [],
}


export const AppDataSource = new DataSource(AppDataSourceOptions)
